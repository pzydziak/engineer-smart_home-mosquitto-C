#!/bin/bash
echo This dir:
git st

printf "\n\n"

echo Subdirs:
find . -maxdepth 1 -type d \( ! -name . \) -exec bash -c "cd '{}' && git st" \;
