# Engineer Project - Linux-based Smart Home 
###### Author - Paweł Żydziak

## What is Linux-based Smart Home?
It is final project for graduate a Engineer degree at Silesian Univerity of Technology.

All connections are based on MQTT lightweight protocol based on TCP. 
The chain of communication looks like this:



    [HOME CLIENT]-------\
                         |------->[SERVER] ------>[UI, probably it would be Android Phone]
    [GARAGE CLIENT]-----/

## Supported features
 v0.1 alpha
 
 * Device Onboarding
 * Reporting of:
    * ACK 
    * Telemetry
    * Alerts
 * Control messages

## Dependencies
| Name | Description |
| ------ | ------ |
| libmosquitto 3.1.1 | MQTT library |
| libjansson | JSON library |
| termios | Serial port communication with arduino |
| WiringPi | Gpio library |

## Compilation
```sh
cd engineer-* # for all directories
make
```

or using helper script

```sh
./make_all
```


## Installation & Running
```sh
cd engineer-*
./<binary_name>
```

## Configuration

### Server
| Configuration Option | Description |
| ------ | ------ |
| downlink_ip | Hostname or IP address of the  MQTT broker for downlink clients  |
| downlink_port | TCP port number of the  MQTT broker |
| uplink_ip | Hostname or IP address of the  MQTT broker for uplink clients |
| uplink_port |  TCP port number of the  MQTT broker |
| keep_alive| Keepalive value for MQTT  |

<br />
### Client
| Configuration Option | Description |
| ------ | ------ |
| uplink_ip | Hostname or IP address of the  MQTT broker for server|
| uplink_port |  TCP port number of the  MQTT broker |
| keep_alive| Keepalive value for MQTT  |

Project specific:

| Configuration Option | Description |
| ------ | ------ |
| Thresholds | High and low thresholds for alerts and control |
| arduino_port | Serialport where to find arduino device |

## Troubleshooting
* TBA

## Testing
Preliminary test were done with the PC and Raspi dev setup 

Testing scope:

 * Device Onboarding
 * Reporting ACK for multiple devices
 * Reporting telemetry for multiple devices
 * Reporting aletrts on Android phone
 * Reciving Control messages

## Known issues

## Roadmap


## Useful Links
 * TBA
 
